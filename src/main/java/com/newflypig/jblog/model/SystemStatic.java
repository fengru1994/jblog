package com.newflypig.jblog.model;

/**
 *	System类的静态方法类，用来在内存中加载System数据 
 *	@author newflypig
 *	time：2015年12月21日
 *
 */
public class SystemStatic {
	public static String BLOG_TITLE;
	public static String BLOG_SCRIPTS;
	public static String BLOG_METAS;
	public static String BLOG_RECORD;
}
