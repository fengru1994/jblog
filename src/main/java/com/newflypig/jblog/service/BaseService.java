package com.newflypig.jblog.service;

import java.io.Serializable;

import org.hibernate.criterion.DetachedCriteria;

import com.newflypig.jblog.io.IOperations;
import com.newflypig.jblog.model.Pager;

public interface BaseService<T extends Serializable> extends IOperations<T> {
	/**
	 * @param dc 离线查询器1,包含了条件、order和limit语法
	 * @param dc2 仅包含条件语法
	 * @param page 当前页面
	 * @return 通用的分页模型
	 */
	public Pager<T> generatePage(DetachedCriteria dc,DetachedCriteria dc2,Integer page);
}
