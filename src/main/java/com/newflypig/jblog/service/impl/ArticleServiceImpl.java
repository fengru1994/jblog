package com.newflypig.jblog.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.newflypig.jblog.dao.ArticleDAO;
import com.newflypig.jblog.dao.BaseDAO;
import com.newflypig.jblog.dao.CommentDAO;
import com.newflypig.jblog.model.Article;
import com.newflypig.jblog.service.ArticleService;


@Service("articleService")
public class ArticleServiceImpl extends BaseServiceImpl<Article> implements ArticleService{

	@Resource(name="articleDao")
	private ArticleDAO articleDao;
	@Resource(name="commentDao")
	private CommentDAO commentDao;
	
	@Override
	protected BaseDAO<Article> getDao() {
		return this.articleDao;
	}
	
	@Override
	@Transactional(propagation=Propagation.NEVER)
	public List<Article> findAllDesc(){
		return this.articleDao.findAllDesc();
	}
}
