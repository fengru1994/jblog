package com.newflypig.jblog.service;

import com.newflypig.jblog.model.System;

public interface SystemService extends BaseService<System>{
	/**
	 * 通过用户名、密码验证鉴权
	 * @return 如果通过则返回System对象，如果不通过返回NULL
	 */
	public System loginByUsernamePwd(String userName,String password);
	
	/**
	 * 获得唯一System对象 
	 */
	public System getSystem();
}
