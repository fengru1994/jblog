package com.newflypig.jblog.util;

/**
 *	静态地存储一些系统配置 
 *	@author newflypig
 *	time：2015年12月3日
 *
 */
public class BlogConfig {
	public final static String CAPTCHA="captcha";
	public final static String PUNISH_TIME="punish_time";
	public final static String POST_NUM="post_num";
	public final static String SYSTEM="system";
	public final static String DESKEY="newflypigadminpasswordmysqldesstringkey";
	
	//登录失败次数的惩罚阀值
	public static int PUNISH_NUM;
	public void setPunishNum(int punishNum){
		BlogConfig.PUNISH_NUM=punishNum;
	}
	
	//登录失败的惩罚分钟数
	public static int PUNISH_MINUTE;
	public void setPunishMinute(int punishMinute){
		BlogConfig.PUNISH_MINUTE=punishMinute;
	}
	
	//分页显示文章列表时的每页个数
	public static int ARTICLES_PER_PAGE;
	public void setArticlesPerPage(int articlesPerPage){
		BlogConfig.ARTICLES_PER_PAGE=articlesPerPage;
	}
	
	//文章最多在URL上显示几页
	public static int ARTICLES_SHOW_PAGE;
	public void setArticlesShowPage(int articlesShowPage){
		BlogConfig.ARTICLES_SHOW_PAGE=articlesShowPage;
	}
	
	//分页显示评论列表时的每页个数
	public static int COMMENTS_PER_PAGE;
	public void setCommentsPerPage(int commentsPerPage){
		BlogConfig.COMMENTS_PER_PAGE=commentsPerPage;
	}
	
	// 评论最多在URL上显示几页
	public static int COMMENTS_SHOW_PAGE;
	public void setCommentsShowPage(int commentsShowPage) {
		BlogConfig.COMMENTS_SHOW_PAGE = commentsShowPage;
	}
	
	/**
	 * 根据实体类获得分页size
	 * @param clazz
	 * @return 分页size
	 */
	@SuppressWarnings("rawtypes")
	public static int getNumberPerPage(Class clazz){
		switch(clazz.getSimpleName()){
		case "Article":
			return BlogConfig.ARTICLES_PER_PAGE;
		case "Comment":
			return BlogConfig.COMMENTS_PER_PAGE;
		default:
			return 10;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static int getNumberShowPage(Class clazz){
		switch(clazz.getSimpleName()){
		case "Article":
			return BlogConfig.ARTICLES_SHOW_PAGE;
		case "Comment":
			return BlogConfig.COMMENTS_SHOW_PAGE;
		default:
			return 9;
		}
	}
}
