package com.newflypig.jblog.dao;

import com.newflypig.jblog.model.System;
/**
 *  定义关于System类的数据库特别操作
 *	time：2015年11月28日
 *
 */
public interface SystemDAO extends BaseDAO<System>{
	/**
	 * 通过用户名、密码验证鉴权
	 * @return 如果通过则返回System对象，如果不通过返回NULL
	 */
	public System loginByUsernamePwd(String userName,String password);
	
	/**
	 * 取得唯一System对象
	 */
	public System getSystem();
}
