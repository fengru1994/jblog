<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.text.SimpleDateFormat"%>
<%@ page language="java" import="com.newflypig.jblog.model.Category"%>
<%@ page language="java" import="com.newflypig.jblog.model.Article"%>
<%@ page language="java" import="com.newflypig.jblog.model.Comment"%>
<%@ page language="java" import="com.newflypig.jblog.exception.JblogException"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/resources/css/common.css">
<script src="<%=request.getContextPath() %>/resources/ueditor/ueditor.parse.min.js"></script>
<title>Article show</title>
</head>
<body>
	<h1>ID:&nbsp;${article.articleId }</h1>
	<h1>Title:&nbsp;${article.title }</h1>
	<h1>Content:&nbsp;</h1>
	<div class="content">${article.text }</div>
	<%
		Article article=(Article)request.getAttribute("article");
		for (Category c : article.getCategories()) {
	%>
		<input type="checkbox" value="<%=c.getCategoryId()%>" name="ctrgs" checked="checked" disabled="disabled"/>
		<span class="category"><%=c.getTitle()%></span>&nbsp;&nbsp;
	<%
		}
	%>
	<br/>
	<%if (article.getPagerComments()!=null) { %>
		<h3>评论列表：</h3>
		<%
			for( Comment comment:article.getPagerComments().getData() ) {
		%>
				<h4>#<%=comment.getFloor()%>楼&nbsp;&nbsp;&nbsp;&nbsp;<%=comment.getNickname()%>&nbsp;&nbsp;&nbsp;&nbsp;<%=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(comment.getDate())%></h4>
				<div class="content">
					<%=comment.getText()%>
				</div>
		<%		
			}
		%>
	
		<div id="pagers" style="margin:10px 0px;"><span>共<%=article.getPagerComments().getTotalPages()%>页：</span>
		<%
			for(String[] url:article.getPagerComments().getUrls()){
		%>		
				<%if(Integer.valueOf(url[1])!=article.getPagerComments().getCurrentPage()){%>
					<span><a href="<%=request.getContextPath()%>/article/<%=article.getArticleId() %>/show?comment_page=<%=url[1]%>"><%=url[0] %></a>
				<%}else{%>
					<span style="color:#5287b1"><%=url[0]%>
				<%}%>
				</span>
		<%
			}
		%>
		</div>
	<%} %>
	
	<form action="/addComment" method="post">
		<input type="hidden" name="articleId" value="<%=article.getArticleId()%>">
		<ul>
			<li><input type="text" id="nickNameId" name="nickname" placeholder="请输入邮箱"></li>
			<li><script id="container" name="text" type="text/plain"></script></li>
			<li>
				<input type="text" name="captcha" placeholder="请输入验证码">
				<img id="captchaImgId" alt="验证码" src="<%=request.getContextPath() %>/captcha-image" width="100px" height="50px" title="点击重置">
				<script type="text/javascript">
					document.getElementById("captchaImgId").onclick=function(e){
						e.target.src="<%=request.getContextPath() %>/captcha-image?m="+Math.random();
					};
				</script>
			</li>
			<li><input type="submit" id="commentSubmitId" value="提交评论"></li>
			<script type="text/javascript">
				document.getElementById("commentSubmitId").onclick=function(e){
					var nickName=document.getElementById("nickNameId").value;
					if(new RegExp("\\w+@(\\w+.)+[a-z]{2,3}").test(nickName))
						return true;
					else{
						alert("Email format is incorrect");
						return false;	
					}					
				};
			</script>
		</ul>
	</form>
	
	<a href="<%=request.getContextPath()%>/article/articles">返回文章列表</a>
	<div class="footer">
		<a href="http://www.miitbeian.gov.cn/" target="blank">苏ICP备15060831号</a>
		<script src="http://s11.cnzz.com/stat.php?id=1256986725&web_id=1256986725" language="JavaScript"></script>
	</div>
</body>
<script type="text/javascript">
uParse('.content', {
    rootPath: "<%=request.getContextPath() %>/resources/ueditor/"
});
</script>
<script type="text/javascript">window.PROJECT_CONTEXT = "<%=request.getContextPath()%>";</script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/ueditor/ueditor.all.js"></script>
<script type="text/javascript">
	var editor = UE.getEditor('container',{
		toolbars: [
			['fullscreen', 'source'],				
		    ['removeformat', 'autotypeset', 'blockquote', 'pasteplain', 'selectall', 'cleardoc','insertcode']
		]
		,initialFrameWidth:460
        ,initialFrameHeight:300
        ,maximumWords:500
	});
</script>
</html>